<?php
class Movie{

	// Database and table names
	private $conn;
	private $table = "movies";

	// Properties
	public $id;
	public $name;
	public $description;
	public $duration;
	public $genere;

	// Constructor eith database connection
	public function __construct($db) {
		$this->conn = $db;
	}	

	function read(){
		$query = "SELECT
					*
				  FROM
				  " . $this->table;
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		return $stmt;
	}

	public function create() {
		$query = "INSERT INTO
		   	" . $this->table . " SET name=:name, description=:description, duration=:duration, genere=:genere";

		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->name=htmlspecialchars(strip_tags($this->name));
	    $this->description=htmlspecialchars(strip_tags($this->description));
	    $this->duration=htmlspecialchars(strip_tags($this->duration));
	    $this->genere=htmlspecialchars(strip_tags($this->genere));

		$stmt->bindParam(":name", $this->name);
		$stmt->bindParam(":description", $this->description);
		$stmt->bindParam(":duration", $this->duration);
		$stmt->bindParam(":genere", $this->genere);

		if ($stmt->execute()) {
			return true;
		}

		return false;
	}

	public function readOne() {
		$query = "SELECT *
					FROM
					" . $this->table;

		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		$this->name = $row['name'];
		$this->description = $row['description'];
		$this->genere = $row['genere'];
	}

	public function update() {

		$query = "UPDATE
			" . $this->table . 
			"SET
				name = :name,
				description = :description,
				duration = :duration
				genere = :genere
			WHERE
				id = :id";

		$stmt = $this->conn->prepare($query);
		$this->name=htmlspecialchars(strip_tags($this->name));
		$this->duration=htmlspecialchars(strip_tags($this->duration));
		$this->description=htmlspecialchars(strip_tags($this->description));
		$this->genere=htmlspecialchars(strip_tags($this->genere));
		$this->id=htmlspecialchars($this->id);

		$stmt->bindParam(":name", $this->name);
		$stmt->bindParam(":description", $this->description);
		$stmt->bindParam(":duration", $this->duration);
		$stmt->bindParam(":genere", $this->genere);
		$stmt->bindParam(":id", $this->id);

		if ($stmt->execute()) {
			return true;
		}

		return false;

	}

	public function delete(){

		$query = "DELETE FROM " . $this->table . " WHERE id = ?";

		$stmt = $this->conn->prepare($query);

		$this->id=htmlspecialchars(strip_tags($this->id));

		$stmt->bindParam(1, $this->id);

		if($stmt->execute()){
			return true;
		}

		return false;
	}
}
?>
