<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Acess-Control-Allow-Headers, Authorization, X-Request-With");

include_once '../config/database.php';
include_once '../objects/movie.php';

$database = new Database();
$db = $database->getConnection();

$movie = new Movie($db);

$data = json_decode(file_get_contents("php://input"));

$movie->id = $data->id;

$movie->description = $data->description;
$movie->duration = $data->duration;
$movie->genere = $data->genere;

if ($movie->update()){

	http_response_code(200);

	echo json_encode(array("message" => "Movie was update"));

} else {
	http_response_code(400);

	echo json_encode(array("message" => "Unable to update"));
}

?>
