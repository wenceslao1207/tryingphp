<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../config/database.php';
include_once '../objects/movie.php';

$database = new Database();
$db = $database->getConnection();

// initializing the movie
$movie = new Movie($db);

$stmt = $movie->read();
$num = $stmt->rowCount();

if ($num>0) {
	
	// Array of movies
	$movies_array=array();
	$movies_array["records"]=array();

	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		extract($row);

		$movie_item=array(
			"id" => $id,
			"name" => $name,
			"description" => $description,
			"duration" => $duration,
			"genere" => $genere
		);

		array_push($movies_array["records"], $movie_item);
	}


	http_response_code(200);

	echo json_encode($movies_array);
}

?>
