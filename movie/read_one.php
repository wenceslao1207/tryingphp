<?php
//require Headers:
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: access *');
header('Access-Control-Allow-Methods: GET');
header('Access-Control-Allow-Credentials: true');
header('Content-Type: application/json');


// imports

include_once '../config/database.php';
include_once '../objects/movie.php';

$database = new Database();
$db = $database->getConnection();

$movie = new Movie($db);

$movie->id = isset($_GET['id']) ? $_GET['id'] : die();

$movie->readOne();

if ($movie->name != null){
	$movieArr = array(
		"id" => $movie->id,
		"name" => $movie->name,
		"description" => $movie->description,
		"genete" => $movie->genere
	);

	http_response_code(200);

	echo json_encode($movieArr);
} else {
	http_response_code(400);

	echo json_encode(array("message" => "Movie does not exist."));
}
?>
