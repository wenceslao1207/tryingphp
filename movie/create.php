<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/database.php';
include_once '../objects/movie.php';

$database = new Database();
$db = $database->getConnection();

$movie = new Movie($db);

$data = json_decode(file_get_contents("php://input"));

if (
	!empty($data->name) &&
	!empty($data->description) &&
	!empty($data->duration) &&
	!empty($data->genere) 
) {


	$movie->name = $data->name;
	$movie->description = $data->description;
	$movie->duration = $data->duration;
	$movie->genere = $data->genere;

	if ($movie->create()){

		http_response_code(201);

		echo json_encode(array("message" => "movie was created"));

	}

	else {

		http_response_code(400);
		echo json_encode(array("message" => "Unable to create a movie data is incomplete"));
	}
}
else {
	http_response_code(400);
	echo json_encode(array("message" => "Unable to create"));
}



?>
