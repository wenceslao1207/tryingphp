CREATE TABLE IF NOT EXISTS movies (
	id int(11) NOT NULL AUTO_INCREMENT,
	name varchar(256) NOT NULL,
	description text NOT NULL,
	duration varchar(20) NOT NULL,
	genere varchar(256) NOT NULL,
	PRIMARY KEY (id)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO movies (id, name, description, duration, genere)
	VALUES (1, 'Empire Strikes Back', 'BEST MOVIE EVER', '2:30', 'Action');
